#!/bin/bash

if [ ! -z $TTT_API_TOKEN ]
then
    API_TOKEN="-authkey ${TTT_API_TOKEN}"
fi

if [ ! -z $TTT_WORKSHOP ]
then
    WORKSHOP="+host_workshop_collection ${TTT_WORKSHOP}"
fi

if [ ! -z $TTT_RCON ]
then
    RCON="+rcon_password ${TTT_RCON}"
fi

if [ ! -z $TTT_MAP ]
then
    RCON="+map ${TTT_MAP}"
fi

cp -n -r /application/default_cfg/* /application/gmodds/garrysmod/cfg/
cp -n -r /application/default_lua/* /application/gmodds/garrysmod/lua/
cp -n -r /application/default_maps/* /application/gmodds/garrysmod/maps/

/application/gmodds/srcds_run \
   -port ${TTT_PORT} \
    -console \
    -condebug \
    -usercon \
    -game garrysmod \
    +maxplayers ${TTT_MAX_PLAYERS} \
    +gamemode terrortown \
    ${WORKSHOP} \
    ${API_TOKEN} \
    ${RCON} \
    