FROM ubuntu:bionic
ENV HOME /application

RUN dpkg --add-architecture i386
RUN apt-get -y update
RUN apt-get -y install lib32gcc1 wget lib32stdc++6

WORKDIR /application

RUN mkdir content gmodds Steam content/css

WORKDIR /application/Steam

RUN wget http://media.steampowered.com/client/steamcmd_linux.tar.gz && \
    tar -xvzf steamcmd_linux.tar.gz && \
    rm steamcmd_linux.tar.gz

RUN ./steamcmd.sh +login anonymous +quit
RUN ./steamcmd.sh +login anonymous \
    +force_install_dir "/application/gmodds" \
    +app_update 4020 validate \
    +quit

RUN ./steamcmd.sh +login anonymous \
    +force_install_dir "/application/content/css" \
    +app_update 232330 \
    +quit

RUN apt-get -y install lib32tinfo5

RUN mkdir -p ~/.steam/sdk32/
RUN ln -s /application/Steam/linux32/ /application/.steam/sdk32

ENV TTT_PORT 27015
ENV TTT_MAX_PLAYERS 16
ENV TTT_IP 0.0.0.0

ADD ./scripts/run.sh /application/run.sh
RUN chmod +x /application/run.sh

RUN cp -R /application/gmodds/garrysmod/cfg /application/default_cfg
RUN cp -R /application/gmodds/garrysmod/lua /application/default_lua
RUN cp -R /application/gmodds/garrysmod/maps /application/default_maps

RUN ln -sf /dev/stdout /application/gmodds/garrysmod/console.log

WORKDIR /application

ENTRYPOINT [ "/application/run.sh" ]